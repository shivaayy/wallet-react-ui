import { Component } from 'react';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css'
import { Navbar, Nav, Button } from 'react-bootstrap'
import {Link} from 'react-router-dom'
import '../index.css'
import wallet from './Img/wallet.png'


class Header extends Component {

    // logoutHandle = () => {
    //     this.props.methods.logoutFun();
    // }
    render() {
        return (<Navbar bg="light" expand="lg">
            <img src={wallet} alt="" className="wallet-img"/>
            <Navbar.Brand>My Wallet</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">

                <Nav className="mr-auto">

                    {/* <Link to="/login" className="p-3">Login</Link> */}

                    {/* <Nav.Link href="/register">Register</Nav.Link> */}
                    {/* <Nav.Link href="/">Login</Nav.Link> */}
                    {this.props.data.isLoggedIn ? <Button data-testid="logout" onClick={e=>{window.confirm("Are you sure?\nYou want to logout!!")&&this.props.methods.logoutFun()}} variant="outline-success" >Logout</Button> :
                    <><Link data-testid="login" to="/" className="text-muted link-style " >Login</Link>
                    <Link data-testid="register" to="/register" className="text-muted link-style " >Register</Link>
                    </>
                     }

                </Nav>



            </Navbar.Collapse>
        </Navbar>);
    }
}

export default Header;