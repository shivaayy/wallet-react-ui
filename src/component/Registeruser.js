import { Component } from "react";
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css'
import { Button, Form, Alert } from 'react-bootstrap'
import axios from 'axios'


class Registeruser extends Component {
    state = {

        userName: "",
        password: "",
        name: "",
        confirmPassword: "",
        passwordNotConfirmed: false,
        showWarningStatus: false,
        warningVariant: "success",
        warningContent: "",

    }
    
    changeHandler = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }
    registerUser = (e) => {
        e.preventDefault();
        if (this.state.password === this.state.confirmPassword) {
            this.setState({ passwordNotConfirmed: false })

            console.log(this.state);

            let url = "http://localhost:8080/register";
            let config = {
                headers: { 'Access-Control-Allow-Origin': '*' }
            };

            axios.post(url, this.state, config)
                .then(msg => {
                    console.log(msg["status"]);
                    console.log(msg);
                    // alert(msg["data"]);
                    const changeStateData = {
                        showWarningStatus: true,
                        warningVariant: "success",
                        warningContent: "new user added, please login to access your wallet"
                    };
                    this.setState(changeStateData);

                })
                .catch(e => {
                    console.log(e);
                    const changeStateData = {
                        showWarningStatus: true,
                        warningVariant: "danger",
                        warningContent: "user already exist!! please enter differnet email"
                    };
                    this.setState(changeStateData);
                });

        }
        else {
            this.setState({ passwordNotConfirmed: true })
        }



    }
    render() {
        return (

            <div className="mx-auto mt-5 w-50 shadow p-3 mb-5 bg-white rounded">
                <h2 className="text-center">Register new wallet user</h2>
                <Form onSubmit={this.registerUser}>
                    <Form.Group>
                        <Form.Label>
                            Full Name
                        </Form.Label>
                        <Form.Control data-testid="name" required autoComplete="off" type="text" placeholder="Full Name" name="name" value={this.state.name} onChange={this.changeHandler} />
                    </Form.Group>


                    <Form.Group>
                        <Form.Label>
                            Email
                        </Form.Label>
                        <Form.Control data-testid="userName" required autoComplete="off" type="email" placeholder="Example@email.com" name="userName" value={this.state.userName} onChange={this.changeHandler} />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>
                            Password
                        </Form.Label>
                        <Form.Control data-testid="password" required autoComplete="off" type="password" placeholder="password" name="password" value={this.state.password} onChange={this.changeHandler} />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>
                            Confirm password
                        </Form.Label>
                        <Form.Control data-testid="confirmPassword" required autoComplete="off" type="password" placeholder="confirm password" name="confirmPassword" value={this.state.confirmPassword} onChange={this.changeHandler} />
                        {this.state.passwordNotConfirmed && <Form.Text className="text-danger">
                            please enter matching password to confirm
                        </Form.Text>}
                    </Form.Group>
                    <Button variant="secondary" type="submit" className="mt-2">Register</Button>
                </Form>
                {this.state.showWarningStatus && <Alert variant={this.state.warningVariant} className="mt-2" >
                    {this.state.warningContent}
                </Alert>}
            </div>
        );
    }

}

export default Registeruser;