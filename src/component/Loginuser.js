import { Component } from "react";
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css'
import { Button, Form, Alert } from 'react-bootstrap'
import axios from 'axios'

class Loginuser extends Component {
    state = {
        userName: "",
        password: "",
        loginUnsuccessful: false
    }

    onChangeHandle = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    login = (e) => {
        e.preventDefault();
        this.setState({loginUnsuccessful:false});
        let url = "http://localhost:8080/login";
        let config = {
            headers: { 'Access-Control-Allow-Origin': '*' }
        };

        axios.post(url, this.state, config)
            .then(msg => {
                console.log(msg["status"]);
                console.log(msg);
                console.log("----username---", msg.data.userName);

                let stateData = {
                    name: msg.data.name,
                    userName: msg.data.userName,
                    balance: msg.data.currentBalance,
                    isLoggedIn: true
                };
                localStorage.setItem("userData", JSON.stringify(stateData));

                this.props.data.stateChangeFun(stateData);


                alert("User authentication successful");


            })
            .catch(e => {
                console.log(e);
                this.setState({loginUnsuccessful:true});
            });




    }

    render() {
        return (
            <div className="mx-auto mt-5 w-50 shadow p-3 mb-5 bg-white rounded">
                <h2 className="text-center">Login to your wallet</h2>
                <Form onSubmit={this.login}>
                    <Form.Group>
                        <Form.Label data-testid="userNameText">
                            Username
                        </Form.Label>
                        <Form.Control data-testid="userName" onChange={this.onChangeHandle} autoComplete="off" required type="email" placeholder="example@email.com" name="userName" value={this.state.userName} />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label data-testid="passwordText">
                            Password
                        </Form.Label>
                        <Form.Control data-testid="password" onChange={this.onChangeHandle} autoComplete="off" required type="password" placeholder="password" name="password" value={this.state.password} />
                    </Form.Group>
                    <Button data-testid="login" variant="secondary" type="submit" className="mt-2 mr-2">Login</Button>
                    <Button data-testid="register" href="/register" variant="outline-success" className="mt-2" style={{ marginLeft: 5 }}>Register new user</Button>
                </Form>
                {this.state.loginUnsuccessful && <Alert variant="danger" className="mt-2">
                    wrong username or password !!
                </Alert>}

            </div>
        );

    }
}

export default Loginuser;