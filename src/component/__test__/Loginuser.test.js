import React from 'react'
import Loginuser from '../Loginuser'
import { render,fireEvent } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'

test("user login form component text testing",()=>{
    const {getByTestId}=render(<Loginuser/>);

    const userNameInputEl=getByTestId("userName");
    const passwordInputEl=getByTestId("password");

    const userNameTextEl=getByTestId("userNameText");
    const passwordTextEl=getByTestId("passwordText");
    
    const loginButtonEl=getByTestId("login");
    const registerButtonEl=getByTestId("register");
    

    expect(userNameInputEl.value).toBe("");
    expect(passwordInputEl.value).toBe("");

    expect(userNameTextEl.textContent).toBe("Username");expect(loginButtonEl.textContent).toBe("Login")
    expect(passwordTextEl.textContent).toBe("Password");

    expect(loginButtonEl.textContent).toBe("Login")
    expect(registerButtonEl.textContent).toBe("Register new user")



})


test("checking onChange event of Username input in login form",()=>{
    const {getByTestId}=render(<Loginuser/>);

    const userNameInputEl=getByTestId("userName");
    

    fireEvent.change(userNameInputEl,{
        target:{
            value:"shivam.yadav@nextuple.com"
        }
    });

    expect(userNameInputEl.value).toBe("shivam.yadav@nextuple.com");

});

test("checking onChange event of password input in login form",()=>{
    const {getByTestId}=render(<Loginuser/>);

    
    const passwordInputEl=getByTestId("password");

    fireEvent.change(passwordInputEl,{
        target:{
            value:"xyz123"
        }
    });

    expect(passwordInputEl.value).toBe("xyz123");

});