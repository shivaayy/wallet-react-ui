import React from 'react'
import Header from '../Header'
import { render } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'




test("logout button render with correct text",()=>{
    const data={isLoggedIn:true};
    const {getByTestId}=render(<Header data={data}/>);
    const buttonEl=getByTestId("logout");

    expect(buttonEl.textContent).toBe("Logout");
})

// test("login link render with correct text",()=>{
//     const data={isLoggedIn:false};
//     const {getByTestId}=render(<Header data={data}/>);
//     const linkEl=getByTestId("login");

//     expect(linkEl.textContent).toBe("Login");

// })

// test("register link render with correct text",()=>{
//     const data={isLoggedIn:false};
//     const {getByTestId}=render(<Header data={data}/>);
//     const linkEl=getByTestId("register");

//     expect(linkEl.textContent).toBe("Register");

// })
