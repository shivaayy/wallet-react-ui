import React from 'react'
import Registeruser from '../Registeruser'
import { render,fireEvent } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'

test("",()=>{
    const {getByTestId}=render();
    expect("").toBe("");
})


test("checking onChange event of name input in registration form",()=>{
    const {getByTestId}=render(<Registeruser/>);

    const nameInputEl=getByTestId("name");
    

    fireEvent.change(nameInputEl,{
        target:{
            value:"Shivam Yadav"
        }
    });

    expect(nameInputEl.value).toBe("Shivam Yadav");

});


test("checking onChange event of Username input in registration form",()=>{
    const {getByTestId}=render(<Registeruser/>);

    const userNameInputEl=getByTestId("userName");
    

    fireEvent.change(userNameInputEl,{
        target:{
            value:"shivam.yadav@nextuple.com"
        }
    });

    expect(userNameInputEl.value).toBe("shivam.yadav@nextuple.com");

});

test("checking onChange event of password input in registration form",()=>{
    const {getByTestId}=render(<Registeruser/>);

    
    const passwordInputEl=getByTestId("password");

    fireEvent.change(passwordInputEl,{
        target:{
            value:"xyz123"
        }
    });

    expect(passwordInputEl.value).toBe("xyz123");

});


test("checking onChange event of confirmPassword input in registration form",()=>{
    const {getByTestId}=render(<Registeruser/>);

    
    const confirmPasswordInputEl=getByTestId("confirmPassword");

    fireEvent.change(confirmPasswordInputEl,{
        target:{
            value:"xyz123"
        }
    });

    expect(confirmPasswordInputEl.value).toBe("xyz123");

});