import { Component, Fragment } from "react";
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css'
import { Button, Form, Table, Toast } from 'react-bootstrap'
import axios from 'axios'
import '../index.css'
import wallet from './Img/wallet.png'

class Dashboard extends Component {
    state = {
        userName: "",
        transferAmount: "",
        rechargeAmount: "",
        transactions: [],
        balance: this.props.data.balance,
        show: false,
        toastColor: "",
        toastContent: "",
        comment: ""

    }

    clearForm = {
        userName: "",
        transferAmount: "",
        rechargeAmount: "",
        comment:""
    }

    hideToast = (e) => {
        this.setState({ show: false, toastContent: "", toastColor: "" });
    }

    getBalance = () => {

        let url = `http://localhost:8080/balance/${this.props.data.userName}`;

        let config = {
            headers: { 'Access-Control-Allow-Origin': '*' },
        };


        axios.get(url, config)
            .then(msg => {
                console.log(msg["status"]);
                console.log(msg);
                this.setState({ balance: msg["data"] });
                let data = JSON.parse(localStorage.getItem("userData"));
                data["balance"] = msg["data"];
                localStorage.setItem("userData", JSON.stringify(data));

            })
            .catch(e => console.log(e));

    }

    transferMoney = (e) => {
        e.preventDefault();
        console.log(this.state);

        let url = "http://localhost:8080/transfer";
        let config = {
            headers: { 'Access-Control-Allow-Origin': '*' }
        };

        let data = {
            fromUser: this.props.data.userName,
            toUser: this.state.userName,
            amount: this.state.transferAmount,
            comment:this.state.comment
        }

        axios.post(url, data, config)
            .then(msg => {
                console.log(msg["status"]);
                console.log(msg);
                this.getBalance();
                // alert(msg["data"]);
                this.setState({ show: true, toastColor: "Success", toastContent: msg["data"] });
            })
            .catch(e => {
                console.log(e);
                this.setState({ show: true, toastColor: "Danger", toastContent: "transaction failed !!" });
                // alert("insufficient balance !! please recharge your wallet !!")
            });

        this.setState(this.clearForm);

    }

    recharge = (e) => {
        e.preventDefault();
        console.log(this.state);

        let url = "http://localhost:8080/recharge";
        let config = {
            headers: { 'Access-Control-Allow-Origin': '*' }
        };
        let data = {
            userName: this.props.data.userName,
            creditAmount: this.state.rechargeAmount

        }
        axios.put(url, data, config)
            .then(msg => {
                console.log(msg["status"]);
                console.log(msg);
                this.getBalance();
                // alert(msg["data"]);
                this.setState({ show: true, toastColor: "Success", toastContent: msg["data"] });
            })
            .catch(e => console.log(e));

        this.setState(this.clearForm);

    }

    onChangeHandler = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }


    dateConverter(timestamp) {
        let time = new Date(timestamp);
        return `${time.getDate()}/${time.getMonth() + 1}/${time.getFullYear()}`;
    }
    viewStatement = () => {

        let url = `http://localhost:8080/statement/${this.props.data.userName}`;

        let config = {
            headers: { 'Access-Control-Allow-Origin': '*' },
        };


        axios.get(url, config)
            .then(msg => {
                console.log(msg["status"]);
                console.log(msg);
                this.setState({ transactions: msg["data"] });
                // alert(msg["data"]);
            })
            .catch(e => console.log(e));
    }
    render() {

        return (
            <Fragment>

                <Toast onClose={this.hideToast} show={this.state.show} delay={5000} className="tost" autohide>
                    <Toast.Header closeButton={false}>
                        <img
                            src={wallet}
                            className="rounded me-2"
                            alt=""
                        />
                        <strong className="me-auto">My Wallet</strong>
                        <small>just now</small>
                    </Toast.Header>
                    <Toast.Body className={this.state.toastColor}>{this.state.toastContent}</Toast.Body>
                </Toast>



                <div className="container w-85 shadow p-3 mt-4">
                    <h1 className="text-center">Wellcome to wallet</h1>
                    <div className="row shadow-sm p-3 m-3 text-muted">
                        <h4 >Name : {this.props.data.name}</h4>
                        <h4>Username : {this.props.data.userName}</h4>
                        <h4>Current Balance : {this.state.balance}</h4>
                        <Button variant="secondary" type="submit" onClick={this.getBalance} >Check Balance</Button>

                    </div>
                    <div className="row m-3">
                        <div className="col-lg-6 shadow-sm p-3">
                            <h1> Transfer Money</h1>
                            <Form onSubmit={this.transferMoney}>
                                <Form.Group>
                                    <Form.Label>
                                        Beneficiary Username
                                    </Form.Label>
                                    <Form.Control autoComplete="off" onChange={this.onChangeHandler} required type="email" placeholder="example@email.com" name="userName" value={this.state.userName} />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>
                                        Transfer amount
                                    </Form.Label>
                                    <Form.Control autoComplete="off" onChange={this.onChangeHandler} required type="number" min="1" step="any" placeholder="amount in rupee" name="transferAmount" value={this.state.transferAmount} />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>
                                        Comment
                                    </Form.Label>
                                    <Form.Control autoComplete="off" onChange={this.onChangeHandler} type="text" placeholder="comment (optional)" name="comment" value={this.state.comment} />
                                </Form.Group>

                                <Button variant="secondary" type="submit" className="mt-2 mr-2">Transfer</Button>

                            </Form>
                        </div>
                        <div className="col-lg-6 shadow-sm p-3">
                            <h1>Wallet Recharge</h1>
                            <Form onSubmit={this.recharge}>
                                <Form.Group>
                                    <Form.Label>
                                        Amount
                                    </Form.Label>
                                    <Form.Control autoComplete="off" onChange={this.onChangeHandler} required type="number" min="1" step="any" placeholder="amount in rupee" name="rechargeAmount" value={this.state.rechargeAmount} />
                                </Form.Group>

                                <Button variant="secondary" type="submit" className="mt-2 mr-2">Recharge</Button>


                            </Form>
                        </div>
                    </div>
                    <div className="row m-3">
                        <Button variant="primary" onClick={this.viewStatement} className="mt-2 mr-2">View Statement</Button>
                        <Table striped bordered hover size="sm" className="mt-2">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Time</th>
                                    <th>Credit</th>
                                    <th>Debit</th>
                                    <th>Balance</th>
                                    <th>Description</th>
                                </tr>
                            </thead>
                            <tbody>

                                {this.state.transactions.map((transaction, index) => (<tr key={transaction.transactionId}>
                                    <td>{index + 1}</td>
                                    <td>{this.dateConverter(transaction.transactionTime)}</td>
                                    <td>{transaction.transactionType === "credit" ? transaction.transactionAmount : ""}</td>
                                    <td>{transaction.transactionType === "debit" ? transaction.transactionAmount : ""}</td>
                                    <td>{transaction.balance}</td>
                                    <td>{transaction.description}</td>

                                </tr>))}

                            </tbody>
                        </Table>

                    </div>


                </div></Fragment>
        );
    }
}

export default Dashboard;