import React from 'react';
import { Component, Fragment } from 'react';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';

import Loginuser from './component/Loginuser';
import Registeruser from './component/Registeruser';
import Header from './component/Header';
import Dashboard from './component/Dashboard';



class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      name: "",
      userName: "",
      balance: "",
      isLoggedIn:true

    }
  }


  changingState = (data) => {
    this.setState(data)

  }

  logout = () => {
    let data = { name: "", userName: "", balance: "", isLoggedIn: false }
    localStorage.setItem("userData", JSON.stringify(data));
    this.setState({ name: "", userName: "", balance: "", isLoggedIn: false }, () => console.log("state changed", this.state))
    // console.log("after set state", this.state)
  }

  render() {
    // console.log("inside render state-----------", this.state);

    let data = JSON.parse(localStorage.getItem("userData"));
    if (data == null) {
      data = { name: "", userName: "", balance: "", isLoggedIn: false };

      localStorage.setItem("userData", JSON.stringify(data));
    }
    
    const context = {
      logoutFun: this.logout,
      stateChangeFun:this.changingState
    };

    return (<Fragment>

      <BrowserRouter>

        <Header data={data} methods={context} />

        <Switch>



          <Route exact path="/register">
            {data.isLoggedIn ? <Redirect to="/dashboard" /> : <Registeruser />}
            {/* <Registeruser /> */}
          </Route>

          <Route exact path="/dashboard">
            {data.isLoggedIn ? <Dashboard data={data} /> : <Redirect to="/" />}
          </Route>

          <Route exact path="/">
            {data.isLoggedIn ? <Redirect to="/dashboard" /> : <Loginuser data={context} />}
            {/* <Loginuser /> */}
          </Route>
        </Switch>

      </BrowserRouter>


    </Fragment>

    );
  }
}


export default App;
